from django.shortcuts import render
from django.views.decorators.csrf import csrf_exempt
from .models import Post
from .serializers import PostSerializer
from django.http import JsonResponse


@csrf_exempt
def posts(request):
    if request.method == 'GET':
        posts = Post.objects.all().order_by('-date')
        serializer = PostSerializer(posts, many=True)
        return JsonResponse(serializer.data, safe=False)
