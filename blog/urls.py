from django.urls import path
from .views import posts


urlpatterns = [
    path('news', posts, name='posts'),
]
